require("dotenv").config()

const auth = {
	type: "OAuth2",
	user: "misnasyari@gmail.com",
	clientId: process.env.CLIENT_ID,
	clientSecret: process.env.CLIENT_SECRET,
	refreshToken: process.env.REFRESH_TOKEN
}

const mailoptions = {
	from: "misnasyari@gmail.com",
	to: "misnasyari@outlook.com",
	subject: "Gmail API NodeJS"
}

module.exports = {
	auth,
	mailoptions
}
